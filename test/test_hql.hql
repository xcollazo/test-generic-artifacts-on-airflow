-- Parameters:
--     destination_directory -- Directory where to write transformation
--                              results
--
-- Usage:
--     spark3-sql --master yarn -f test_hql.hql.                                                  \
--         -d destination_directory=/tmp/xcollazo/example

SET spark.hadoop.hive.exec.compress.output=false;

INSERT OVERWRITE DIRECTORY "${destination_directory}"
    USING csv
    OPTIONS ('compression' 'uncompressed', 'sep' ' ')

    SELECT 100 AS c1
;
